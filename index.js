const fs = require("fs");
const archiver = require("archiver");
const path = require("path");
const { createJsonFile } = require("./createJSON");

let index = 0;

function zipFolders(rootFolder) {
  //   create a new archive
  const archive = archiver("zip", {
    zlib: { level: 9 }, // Sets the compression level.
  });
  const addFile = (file) => {
    return new Promise((resolve) => {
      archive.file(file, { name: file });
      if (index === 1000) {
        archive.finalize();
      }
      resolve();
    });
  };
  // look for all subfolders\
  fs.readdirSync(rootFolder).forEach((folder) => {
    const folderPath = path.join(rootFolder, folder);
    if (fs.lstatSync(folderPath).isDirectory()) {
      fs.readdirSync(folderPath).forEach((childFolder) => {
        let logo = "";

        const childPath = path.join(folderPath, childFolder);
        if (index === 5) return;
        // check is logo exist
        if (fs.existsSync(`${childPath}/logo.jpg`)) {
          fs.renameSync(`${childPath}/logo.jpg`, `./logo/${childFolder}.jpg`);
          logo = `https://gitlab.com/tranhuy.beearning/gameboy-advance-games/-/raw/main/logo/${childFolder}.jpg`;
          console.log(`File moved successfully. ${childPath}`);
        } else if (fs.existsSync(`./logo/${childFolder}.jpg`)) {
          logo = `https://gitlab.com/tranhuy.beearning/gameboy-advance-games/-/raw/main/logo/${childFolder}.jpg`;
          console.log(`Has Logo-----> ${childPath}`);
        }
        createJsonFile(childFolder, logo);
        if (fs.existsSync(`./zip/${childFolder}.zip`)) {
          console.log(`./zip/${childFolder}.zip already exist !!!!!`);
          return;
        }
        if (fs.existsSync(`./logo/${childFolder}.jpg`)) {
          console.log(`./logo/${childFolder}.jpg already exist !!!!!`);
          return;
        }
        // creates a new file with the name archiveName
        const output = fs.createWriteStream(path.join("./zip", `${childFolder}.zip`));

        // pipe archive data to the file
        archive.pipe(output);

        console.log({ childPath });
        index++;
        //   add all files inside the subfolder to the archive
        archive.directory(childPath, childFolder);
      });
    }
  });
}

async function archiveFolder(rootFolder) {
  // create a promise array to handle async process
  const promises = [];

  // get all subfolders within the root folder
  fs.readdirSync(rootFolder, { withFileTypes: true })
    .filter((dirent) => dirent.isDirectory())
    .forEach((dirent) => {
      const folder = dirent.name;
      const folderPath = path.join(rootFolder, folder);
      fs.readdirSync(folderPath).forEach((childFolder) => {
        let logo = "";

        const childPath = path.join(folderPath, childFolder);
        console.log(childPath);
        if (index === 5) return;
        index++;
        // check is logo exist
        if (fs.existsSync(`${childPath}/logo.jpg`)) {
          fs.renameSync(`${childPath}/logo.jpg`, `./logo/${childFolder}.jpg`);
          logo = `https://gitlab.com/tranhuy.beearning/gameboy-advance-games/-/raw/main/logo/${childFolder}.jpg`;
          console.log(`File moved successfully. ${childPath}`);
        } else if (fs.existsSync(`./logo/${childFolder}.jpg`)) {
          logo = `https://gitlab.com/tranhuy.beearning/gameboy-advance-games/-/raw/main/logo/${childFolder}.jpg`;
          console.log(`Has Logo-----> ${childPath}`);
        }
        createJsonFile(childFolder, logo);
        if (fs.existsSync(`./zip/${childFolder}.zip`)) {
          console.log(`./zip/${childFolder}.zip already exist !!!!!`);
        } else {
          return;
          promises.push(
            new Promise(async (resolve, reject) => {
              // create a new archive
              const archive = archiver("zip", {
                zlib: { level: 9 },
              });

              // specify the output file
              const output = fs.createWriteStream(path.join("./zip", `${childFolder}.zip`));

              // listen for errors
              archive.on("error", (err) => {
                reject(err);
              });

              // pipe the archive to the output file
              archive.pipe(output);

              // get all files within the childFolder
              archive.directory(childPath, childFolder);

              // finalize the archive
              archive.finalize();

              // listen for the 'close' event, which indicates that the archive is complete
              output.on("close", () => {
                console.log(`Archive for ${childFolder} created: ${archive.pointer()} total bytes`);
                resolve();
              });
            })
          );

          // // creates a new file with the name archiveName
          // const output = fs.createWriteStream(path.join("./zip", `${childFolder}.zip`));

          // // pipe archive data to the file
          // archive.pipe(output);

          // console.log({ childPath });
          // index++;
          // //   add all files inside the subfolder to the archive
          // archive.directory(childPath, childFolder);
        }
      });
    });

  //   loop through each subfolder
  //   for (const subfolder of subfolders) {

  //   }
  console.log({ promises });

  await Promise.all(promises);
  console.log("All archive done!");
}

archiveFolder("./gameboy-advance");

// zipFolders("./gameboy-advance");
