const fs = require("fs");

const replaceSpace = (str) => {
  str = str.replace(/\s+/g, "");
  return str;
};

// const handleReadFile = (pathFolder) => {
//   fs.readdir(pathFolder, (err, files) => {
//     files.forEach((file) => {
//       readFileJson(`${pathFolder}/${file}`);
//     });
//   });
// };

const readFileJson = (pathFile) => {
  fs.readFile(pathFile, "utf8", function (err, data) {
    if (err) {
      console.log("🚀 ~ file: createJsonfile.js ~ line 6 ~ err", err);
      return;
    }
    let file = JSON.parse(data);
    let { name, logo, author } = file;
    createJsonFile(name, logo);
  });
};

function appendDataToJsonFile(filePath, newData) {
  // Read the existing JSON file
  const data = fs.readFileSync(filePath, "utf8");

  // Parse the JSON data
  let jsonData = JSON.parse(data);
  if (Array(jsonData).some((e) => e.id === newData.id)) {
    console.log(`${newData.id} already exist!!!!!!!!! in jsonData`);
    return;
  }
  // Add the new data to the existing JSON object
  jsonData.push(newData);

  // Stringify the updated JSON object
  let jsonString = JSON.stringify(jsonData);

  // Write the updated JSON string to the file
  fs.writeFileSync(filePath, jsonString);
}

const createJsonFile = (nameGame, logoGame) => {
  //   const foldername = "./";

  let obj = {
    name: nameGame,
    version: "0.0.1",
    hash: "335d3df52df4852c962277ca563e4a7cb67f450a31f9b3fe80543f262bf3f71c",
    pubKey: "927c8f340255d5a2d7b839ce4859f7dc33c64e5967fe40dde023c0751a53dff3925eec1eaaeb60f83c203c85e9c5b223",
    sign: "a72c0b66b936b36525fdc1f07c94266a8280abd6acb04d1d87477d8ea790c98593084649f5be7fb7653c8ffcc34dd3e5067b2ffac7feaa85e5e574c972b7363dc0ac7df937c82cdce2ccae7cf6c2df265a3e8c9188cda23d872da08a0e2fa5a4",
    urlLauchScreen: "",
    urlLoadingScreen: "",
    urlZip: `https://gitlab.com/tranhuy.beearning/gameboy-advance-games/-/raw/main/zip/${replaceSpace(nameGame)}`,
    logo: logoGame,
    id: `${replaceSpace(nameGame)}.game`,
    orientation: "landscape",
    author: "n/a",
    full_screen: true,
    status_bar: "hide",
  };

  // let json = JSON.stringify(obj);
  appendDataToJsonFile(`./json/gameboy_advance_1.0.0.json`, obj);

  // fs.appendFile(`./json/gameboy_advance_1.0.0.json`, json + ", \n", function (err) {
  //   if (err) {
  //     console.log("err addfile json -->", err);
  //     return;
  //   }
  //   console.log("addfile json complete");
  // });
};

const directoryPath = "zip";
const jsonFilePath = "json/gameboy_advance_1.0.0.json";

let newArr = [];

fs.readFile(jsonFilePath, "utf8", function (err, data) {
  if (err) throw err;

  const json = JSON.parse(data);
  const filenames = Object.keys(json);

  for (const index in json) {
    const filename = json[index];
    if (!filename?.name) continue;
    if (!fs.existsSync(`${directoryPath}/${filename.name}.zip`)) {
      // remove the property if its name doesn't exist as a file in the directory
      console.log(`Object with key "${filename.name}" removed from the JSON file.`);
    } else {
      filename.urlZip = `https://gitlab.com/tranhuy.beearning/gameboy-advance-games-2/-/raw/main/zip/${replaceSpace(
        filename.name
      )}`;
      newArr.push(filename);
      console.log(`Object with key "${filename.name}" updated.`);
    }
  }

  // write the updated JSON object to the file
  fs.writeFile(jsonFilePath, JSON.stringify(newArr), "utf8", function (err) {
    if (err) throw err;
    console.log("JSON file updated successfully!");
  });
});

module.exports = { createJsonFile };

// for (const key in arrayLink) {
//   if (Object.hasOwnProperty.call(arrayLink, key)) {
//     const element = arrayLink[key];
//     createJsonFile(element);
//   }
// }
